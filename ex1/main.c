/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on 2017. szeptember 19., 15:29
 */

#include <stdio.h>
#include <stdlib.h>
#include "rw.h"

/*
 * 
 */
int main(int argc, char** argv) {

    char matrix[10][10];
    init(matrix);
    randomwalk(matrix);
    show(matrix);
    
    //getchar();

    return (EXIT_SUCCESS);
}

