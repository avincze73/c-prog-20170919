#include "rw.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void init(char matrix[][10]) {
    srand(time(NULL));
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            matrix[i][j] = '.';
        }
    }
}

void randomwalk(char matrix[][10]) {
    int x = 5;
    int y = 5;
    char lastchar = 'a';
    matrix[5][5] = lastchar;
    int randomnumber;
    while (1) {
        randomnumber = rand() % 4;
        int oldx = x;
        int oldy = y;
        switch (randomnumber) {
            case 0:
                --y;
                break;

            case 1:
                --x;
                break;

            case 2:
                ++y;
                break;

            case 3:
                ++x;
                break;

        }
        if (matrix[x][y] != '.') {
            x = oldx;
            y = oldy;
            continue;
        }

        if (x == 10 || y == 10 || x == -1 || y == -1) {
            break;
        }
        oldx = x;
        oldy = y;
        matrix[x][y] = ++lastchar;
    }
}

void show(char matrix[][10]) {
    //int m = 2, n = 3;
    //int a[m][n];
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            printf("%c ", matrix[i][j]);
        }
        printf("\n");
    }
}