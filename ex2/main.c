/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on 2017. szeptember 20., 10:15
 */

#include <stdio.h>
#include <stdlib.h>
#include "A.h"
#include "B.h"
#include <stdbool.h>
#include <string.h>
/*
 * 
 */

int accum(int* a, int length, int init)
{
    for(int i = 0; i < length; ++i)
    {
        init+=i[a];
    }
    return init;
}

int accum2(int* begin, int* end, int init,
        int(*policy)(int, int), bool(*predicate)(int))
{
    for(;begin != end; ++begin)
    {
        if (predicate(*begin)) {
            init = policy(*begin,init);
        }
    }
    return init;
}

int Add(int a, int b)
{
    return a + b;
}
bool Even(int a)
{
    return a % 2 == 0;
}

int main(int argc, char** argv) {
    
    int a[]={1,2,3,4,5,6,7,8,9,10};
    printf("%d\n", accum((int[]){1,2,3,4,5,6,7,8,9,10}, 10, 0));
    printf("%d\n", accum(a, 10, 0));
    printf("%d\n", accum2(a, a+10, 0, Add, Even));
    
    return (EXIT_SUCCESS);
}

