/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on 2017. szeptember 20., 12:00
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int str_compare(const void* s1, const void* s2)
{
    return strcmp((const char*) s1, (const char*) s2);
}
bool is_anagram(const char* s1, const char* s2) {
    if (strlen(s1) != strlen(s2)) {
        return false;
    }
    char s1a[strlen(s1)+1];
    strcpy(s1a,s1);
    char s2a[strlen(s2)+1];
    strcpy(s2a,s2);
    qsort(s1a, strlen(s1a), sizeof(char), str_compare);
    qsort(s2a, strlen(s2a), sizeof(char), str_compare);
    return strcmp(s1a, s2a) == 0 ? true : false;
}

void get_days(char*** days)
{
    static char* arg[]={"sunday", "monday", "tuesday",
    "wednesday", "thursday", "friday", "saturday"}; 
    *days = arg;
}

int main(int argc, char** argv) {
    
    printf("%s\n", is_anagram("abcd", "bacd") ? "yes" : "no");
    char** days;
    get_days(&days);
    printf("%s", *(days + 2));
    return (EXIT_SUCCESS);
}

